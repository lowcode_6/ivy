# ivy-el-parser

#### 介绍
基于LiteFlow规则引擎，把前端LogicFlow数据解析成EL表达式的解析器。
支持代码中动态构建LogicFlow数据格式。


#### 待办

- [x] 动态构建流程数据
- [x] 流程数据解析成表达式
- [x] 发布到maven中央仓库
- [ ] 支持与或非表达式



#### 引入依赖
```
<dependency>
    <groupId>io.gitee.yunzy</groupId>
    <artifactId>ivy-el-parser</artifactId>
    <version>1.0.0</version>
</dependency>
```

#### 使用说明

1.  构建json数据
```
    public static void main(String[] args) {
        //构建节点属性数据
        IvyFlowBuilderNodeProp nodeProp1 = IvyFlowBuilderNodeProp.NEW().componentId("a").componentName("组件a").nodeType(NodeTypeEnum.COMMON);
        IvyFlowBuilderNodeProp nodeProp2 = IvyFlowBuilderNodeProp.NEW().componentId("b").componentName("组件b").nodeType(NodeTypeEnum.COMMON);
        IvyFlowBuilderNodeProp nodeProp3 = IvyFlowBuilderNodeProp.NEW().componentId("c").componentName("组件c").nodeType(NodeTypeEnum.COMMON);
        //构建节点数据(普通组件)
        IvyFlowBuilderNode node1 = IvyFlowBuilderNode.NEW().id().type(NodeTypeEnum.COMMON).properties(nodeProp1);
        IvyFlowBuilderNode node2 = IvyFlowBuilderNode.NEW().id().type(NodeTypeEnum.COMMON).properties(nodeProp2);
        IvyFlowBuilderNode node3 = IvyFlowBuilderNode.NEW().id().type(NodeTypeEnum.COMMON).properties(nodeProp3);
        //构建连线属性数据
        IvyFlowBuilderEdgeProp edgeProp = IvyFlowBuilderEdgeProp.NEW().linkType(0).id("id").tag("tag");
        //构建连线数据
        IvyFlowBuilderEdge edge1 = IvyFlowBuilderEdge.NEW()
                .sourceNode(node2.getNode().getId())
                .targetNode(node3.getNode().getId())
                .text("").properties(edgeProp);

        String json = IvyFlowBuilder.builderJson()
                .addNode(node1,node2,node3)
                .addEdge(node1,node2)
                .addEdge(edge1)
                .toFormatJson();

        System.out.println(json);
    }
```
生成json
```
{
  "nodes": [
    {
      "id": "650744dd-20fc-46ef-be76-01e9c6fbd99a",
      "type": "common",
      "properties": {
        "componentId": "a",
        "componentName": "组件a",
        "type": "common",
        "nodeType": "COMMON"
      }
    },
    {
      "id": "39be41da-7b52-4841-b7ab-b189f8d4820a",
      "type": "common",
      "properties": {
        "componentId": "b",
        "componentName": "组件b",
        "type": "common",
        "nodeType": "COMMON"
      }
    },
    {
      "id": "30d501ab-c5ed-477c-9b5c-08476003787b",
      "type": "common",
      "properties": {
        "componentId": "c",
        "componentName": "组件c",
        "type": "common",
        "nodeType": "COMMON"
      }
    }
  ],
  "edges": [
    {
      "sourceNodeId": "650744dd-20fc-46ef-be76-01e9c6fbd99a",
      "targetNodeId": "39be41da-7b52-4841-b7ab-b189f8d4820a",
      "properties": {
        "linkType": 0
      },
      "text": ""
    },
    {
      "sourceNodeId": "39be41da-7b52-4841-b7ab-b189f8d4820a",
      "targetNodeId": "30d501ab-c5ed-477c-9b5c-08476003787b",
      "properties": {
        "id": "id",
        "tag": "tag",
        "linkType": 0
      },
      "text": ""
    }
  ]
}
```
2.  解析json数据

```
String el = IvyFlowParser.parse(json);
```
生成el表达式
```
THEN(node("a"),node("b"),node("c"));
```