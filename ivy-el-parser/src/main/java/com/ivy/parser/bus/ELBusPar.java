package com.ivy.parser.bus;

import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ParELWrapper;

public class ELBusPar extends BaseELBus {

    public static ParELWrapper node(Object... objects){
        return ELBus.par(objects);
    }

//    public static ParELWrapper node(IvyCmp cmp){
//        ParELWrapper wrapper = ELBus.par(cmp.getComponentId());
//        setId(wrapper, cmp);
//        setTag(wrapper, cmp);
//        setMaxWaitSeconds(wrapper, cmp);
//        return wrapper;
//    }

}
