package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.*;

public class ELBusWhile extends BaseELBus {

    public static LoopELWrapper node(NodeELWrapper nodeElWrapper) {
        return ELBus.whileOpt(nodeElWrapper);
    }

    public static LoopELWrapper node(String nodeElWrapper) {
        return ELBus.whileOpt(nodeElWrapper);
    }

    public static LoopELWrapper node(AndELWrapper andElWrapper) {
        return ELBus.whileOpt(andElWrapper);
    }

    public static LoopELWrapper node(OrELWrapper orElWrapper) {
        return ELBus.whileOpt(orElWrapper);
    }

    public static LoopELWrapper node(NotELWrapper notElWrapper) {
        return ELBus.whileOpt(notElWrapper);
    }

    public static LoopELWrapper node(IvyCmp cmp){
        Object doOpt = cmp.getCmpDoOptEL() != null ? cmp.getCmpDoOptEL() : cmp.getCmpDoOpt();
        Object breakOpt = cmp.getCmpBreakOptEL() != null ? cmp.getCmpBreakOptEL() : cmp.getCmpBreakOpt();
        LoopELWrapper wrapper = ELBus.whileOpt(ELBusNode.node(cmp));
        wrapper.doOpt(doOpt);
        if(breakOpt != null){
            wrapper.breakOpt(breakOpt);
        }
        if(cmp.getCmpParallel() != null){
            wrapper.parallel(cmp.getCmpParallel());
        }
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        setMaxWaitSeconds(wrapper, cmp);
        return wrapper;
    }

}
