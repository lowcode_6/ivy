package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.FinallyELWrapper;

public class ELBusFinally extends BaseELBus {

    public static FinallyELWrapper node(Object... object){
        return new FinallyELWrapper(object);
    }

    public static FinallyELWrapper node(IvyCmp cmp){
        FinallyELWrapper wrapper = new FinallyELWrapper(cmp.getFinallyEL());
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        return wrapper;
    }
}
