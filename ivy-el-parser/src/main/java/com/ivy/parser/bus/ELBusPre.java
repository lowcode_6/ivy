package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.PreELWrapper;

public class ELBusPre  extends BaseELBus {

    public static PreELWrapper node(Object... object){
        return new PreELWrapper(object);
    }

    public static PreELWrapper node(IvyCmp cmp){
        PreELWrapper wrapper = new PreELWrapper(cmp.getPreEL());
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        setMaxWaitSeconds(wrapper, cmp);
        return wrapper;
    }

}
