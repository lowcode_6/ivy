package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.AndELWrapper;
import com.yomahub.liteflow.builder.el.CatchELWrapper;
import com.yomahub.liteflow.builder.el.ELBus;

public class ELBusCatch extends BaseELBus {

    public static CatchELWrapper catchException(Object object){
        return ELBus.catchException(object);
    }

    public static CatchELWrapper catchException(IvyCmp cmp){
        CatchELWrapper wrapper = ELBus.catchException(cmp.getCatchEL());
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        setMaxWaitSeconds(wrapper, cmp);
        setDoOpt(wrapper, cmp);
        return wrapper;
    }
}
