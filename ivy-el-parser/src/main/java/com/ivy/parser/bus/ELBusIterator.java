package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.LoopELWrapper;
import com.yomahub.liteflow.builder.el.NodeELWrapper;

public class ELBusIterator extends BaseELBus {

    public static LoopELWrapper node(NodeELWrapper nodeElWrapper) {
        return ELBus.iteratorOpt(nodeElWrapper);
    }

    public static LoopELWrapper node(String nodeElWrapper) {
        return ELBus.iteratorOpt(nodeElWrapper);
    }

    public static LoopELWrapper node(IvyCmp cmp){
        Object doOpt = cmp.getCmpDoOptEL() != null ? cmp.getCmpDoOptEL() : cmp.getCmpDoOpt();
        Object breakOpt = cmp.getCmpBreakOptEL() != null ? cmp.getCmpBreakOptEL() : cmp.getCmpBreakOpt();
        LoopELWrapper wrapper = ELBus.iteratorOpt(ELBusNode.node(cmp));
        wrapper.doOpt(doOpt);
        if(breakOpt != null){
            wrapper.breakOpt(breakOpt);
        }
        if(cmp.getCmpParallel() != null){
            wrapper.parallel(cmp.getCmpParallel());
        }
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        setMaxWaitSeconds(wrapper, cmp);
        return wrapper;
    }

}
