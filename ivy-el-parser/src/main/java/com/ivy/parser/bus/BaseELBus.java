package com.ivy.parser.bus;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.*;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

public class BaseELBus {

    protected static void setDoOpt(ELWrapper wrapper, IvyCmp cmp){
        Object doOpt = cmp.getCmpDoOptEL() != null ? cmp.getCmpDoOptEL() : cmp.getCmpDoOpt();
        setDoOpt(wrapper, doOpt);
    }

    protected static void setDoOpt(ELWrapper wrapper, Object doOpt){
        if(wrapper instanceof CatchELWrapper){
            CatchELWrapper elWrapper = (CatchELWrapper) wrapper;
            elWrapper.doOpt(doOpt);
        }
    }

    protected static void setId(ELWrapper wrapper, IvyCmp cmp){
        setId(wrapper, cmp.getCmpId());
    }

    protected static void setId(ELWrapper wrapper, String id){
        if(wrapper instanceof NodeELWrapper){
            ((NodeELWrapper) wrapper).id(id);
        }else if(wrapper instanceof AndELWrapper){
            ((AndELWrapper) wrapper).id(id);
        }else if(wrapper instanceof CatchELWrapper){
            ((CatchELWrapper) wrapper).id(id);
        }else if(wrapper instanceof FinallyELWrapper){
            ((FinallyELWrapper) wrapper).id(id);
        }else if(wrapper instanceof PreELWrapper){
            ((PreELWrapper) wrapper).id(id);
        }else if(wrapper instanceof NotELWrapper){
            ((NotELWrapper) wrapper).id(id);
        }else if(wrapper instanceof OrELWrapper){
            ((OrELWrapper) wrapper).id(id);
        }else if(wrapper instanceof WhenELWrapper){
            ((WhenELWrapper) wrapper).id(id);
        }else if(wrapper instanceof ThenELWrapper){
            ((ThenELWrapper) wrapper).id(id);
        }else if(wrapper instanceof LoopELWrapper){
            ((LoopELWrapper) wrapper).id(id);
        }
    }

    protected static void setTag(ELWrapper wrapper, IvyCmp cmp){
        setTag(wrapper, cmp.getCmpTag());
    }

    protected static void setTag(ELWrapper wrapper, String tag){
        if(wrapper instanceof NodeELWrapper){
            ((NodeELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof AndELWrapper){
            ((AndELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof CatchELWrapper){
            ((CatchELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof FinallyELWrapper){
            ((FinallyELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof PreELWrapper){
            ((PreELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof NotELWrapper){
            ((NotELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof OrELWrapper){
            ((OrELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof WhenELWrapper){
            ((WhenELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof ThenELWrapper){
            ((ThenELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof LoopELWrapper){
            ((LoopELWrapper) wrapper).tag(tag);
        }
    }

    protected static void setMaxWaitSeconds(ELWrapper wrapper, IvyCmp cmp){
        setMaxWaitSeconds(wrapper, cmp.getCmpMaxWaitSeconds());
    }

    protected static void setMaxWaitSeconds(ELWrapper wrapper, Integer maxWaitSeconds){
        if(wrapper instanceof NodeELWrapper){
            ((NodeELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof AndELWrapper){
//            ((AndELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof CatchELWrapper){
            ((CatchELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof PreELWrapper){
            ((PreELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof NotELWrapper){
//            ((NotELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof OrELWrapper){
//            ((OrELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof WhenELWrapper){
            ((WhenELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof ThenELWrapper){
            ((ThenELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof LoopELWrapper){
            ((LoopELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }
    }

    protected static void setData(NodeELWrapper wrapper, IvyCmp cmp){
        setData(wrapper, cmp.getCmpDataName(), cmp.getCmpData());
    }

    protected static void setData(NodeELWrapper wrapper, String dataName, String jsonData){
        if(StrUtil.isNotEmpty(jsonData)){
            try {
                Gson gson = new Gson();
                Type type;
                Object obj = null;
                if (jsonData.startsWith("{") && jsonData.endsWith("}")) {
                    type = new TypeToken<Map<String, Object>>(){}.getType(); // 获取Map的Type
                    obj = gson.fromJson(jsonData, type);
                } else if (jsonData.startsWith("[") && jsonData.endsWith("]")) {
                    type = new TypeToken<List<Map<String, Object>>>(){}.getType(); // 获取List的Type
                    obj = gson.fromJson(jsonData, type);
                }
            } catch (Exception e) {
                System.err.println("ELBusNode: Invalid JSON format.");
                System.err.println("dataName: " + dataName);
                System.err.println("jsonData: " + jsonData);
            }
            wrapper.data(dataName, jsonData);
        }
    }

    protected static void setData(NodeELWrapper wrapper, String dataName, Object data){
        if(data != null){
            wrapper.data(dataName, data);
        }
    }

    protected static void setData(NodeELWrapper wrapper, String dataName, Map<String, Object> jsonMap){
        if(CollUtil.isNotEmpty(jsonMap)){
            wrapper.data(dataName, jsonMap);
        }
    }
}
