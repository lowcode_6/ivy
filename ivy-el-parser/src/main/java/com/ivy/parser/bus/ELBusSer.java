package com.ivy.parser.bus;

import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.SerELWrapper;

public class ELBusSer extends BaseELBus {

    public static SerELWrapper node(Object... objects){
        return ELBus.ser(objects);
    }

//    public static SerELWrapper node(IvyCmp cmp){
//        SerELWrapper wrapper = ELBus.ser(cmp.getComponentId());
//        setId(wrapper, cmp);
//        setTag(wrapper, cmp);
//        setMaxWaitSeconds(wrapper, cmp);
//        return wrapper;
//    }

}
