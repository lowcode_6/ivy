package com.ivy.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.ivy.parser.execption.LiteFlowELException;
import com.ivy.parser.logicflow.LogicFlow;

import java.util.Set;

public class IvyFlowParser {

    public static String parse(String json) throws LiteFlowELException {
        return LogicFlow.NEW().json(json).transform().buildEL();
    }

    public static String parse(String json, boolean formatEL) throws LiteFlowELException {
        return LogicFlow.NEW().json(json).transform().buildEL(formatEL);
    }

//    public static void parse(LogicFlowData flowData){
//        return LogicFlow.NEW().flowData(flowData).transform().buildEL();
//    }

    public static Set<JsonNode> getScriptNodes(String json) throws LiteFlowELException {
        return LogicFlow.NEW().json(json).getScriptNodes();
    }

}
