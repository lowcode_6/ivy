package com.ivy.builder.json;


import com.ivy.builder.graph.Edge;
import com.ivy.builder.graph.EdgeProperties;

public class IvyFlowBuilderEdge {

    private Edge edge;

    public IvyFlowBuilderEdge() {
        this.edge = new Edge();
        this.edge.setProperties(new EdgeProperties());
    }

    public static IvyFlowBuilderEdge NEW(){
        return new IvyFlowBuilderEdge();
    }

    public IvyFlowBuilderEdge sourceNode(String sourceNodeId){
        edge.setSourceNodeId(sourceNodeId);
        return this;
    }

    public IvyFlowBuilderEdge targetNode(String targetNodeId){
        edge.setTargetNodeId(targetNodeId);
        return this;
    }

    public IvyFlowBuilderEdge text(String text){
        edge.setText(text);
        return this;
    }

    public IvyFlowBuilderEdge properties(IvyFlowBuilderEdgeProp edgeProp){
        edge.setProperties(edgeProp.getProperties());
        return this;
    }

    public Edge getEdge() {
        return edge;
    }
}
