package com.ivy.builder.json;


import com.ivy.builder.graph.*;
import com.yomahub.liteflow.enums.NodeTypeEnum;

import java.util.List;
import java.util.UUID;

public class IvyFlowBuilderNode {

    private Node node;

    public IvyFlowBuilderNode() {
        this.node = new Node();
        this.node.setProperties(new NodeProperties());
    }

    public static IvyFlowBuilderNode NEW(){
        return new IvyFlowBuilderNode();
    }

    public IvyFlowBuilderNode id(){
        id(UUID.randomUUID().toString());
        return this;
    }

    public IvyFlowBuilderNode id(String id){
        node.setId(id);
        return this;
    }

    public IvyFlowBuilderNode type(NodeTypeEnum nodeTypeEnum) {
        node.setType(nodeTypeEnum.getCode());
        return this;
    }

    public IvyFlowBuilderNode text(String text) {
        node.setText(text);
        return this;
    }

    public IvyFlowBuilderNode children(List<String> children) {
        node.setChildren(children);
        return this;
    }

    public IvyFlowBuilderNode properties(IvyFlowBuilderNodeProp prop){
        node.setProperties(prop.getProperties());
        return this;
    }

    public Node getNode(){
        return this.node;
    }

}
