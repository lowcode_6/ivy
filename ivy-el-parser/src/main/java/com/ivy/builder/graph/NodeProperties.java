package com.ivy.builder.graph;

import lombok.Data;

@Data
public class NodeProperties extends IvyCmp {

    private String[] ids;
    private Boolean ignoreType;

    private Long fallbackCommonId;
    private Long fallbackSwitchId;
    private Long fallbackIfId;
    private Long fallbackForId;
    private Long fallbackWhileId;
    private Long fallbackBreakId;
    private Long fallbackIteratorId;

}
