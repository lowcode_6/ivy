package com.ivy.builder.graph;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Node implements Serializable {

    private String id;
    private String type;
    private NodeProperties properties;
    private String text;
    List<String> children;

}
