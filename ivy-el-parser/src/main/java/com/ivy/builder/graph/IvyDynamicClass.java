package com.ivy.builder.graph;

import com.ivy.anno.Describe;
import com.ivy.anno.DescribeItem;
import lombok.Data;

@Data
public class IvyDynamicClass {

    private Long id;

    private String classId;

    @Describe(value = "class类型",items = {
            @DescribeItem(value = "0",desc = "普通类【class】"),
            @DescribeItem(value = "1",desc = "普通组件类【common】"),
            @DescribeItem(value = "2",desc = "选择组件类【switch】"),
            @DescribeItem(value = "3",desc = "条件组件类【if】"),
            @DescribeItem(value = "4",desc = "次数循环组件类【for】"),
            @DescribeItem(value = "5",desc = "条件循环组件类【while】"),
            @DescribeItem(value = "6",desc = "迭代循环组件类【iterator】"),
            @DescribeItem(value = "7",desc = "退出循环组件类【break】"),
    })
    private Integer classType;

    @Describe(value = "是否为降级组件",items = {
        @DescribeItem(value = "0",desc = "非降级组件"),
        @DescribeItem(value = "1",desc = "降级组件"),
    })
    private Integer isFallback;

    private String packagePath;

    private String className;

    @Describe(value = "加载器类型",items = {
        @DescribeItem(value = "0",desc = "应用程序类加载器"),
        @DescribeItem(value = "1",desc = "自定义类加载器"),
    })
    private Integer classLoaderType;

    private String sourceCode;

    private Integer version;

    private String sourceCmpId;

    private String sourceClassName;

}
