/*
 * Copyright (c) zhg2yqq Corp.
 * All Rights Reserved.
 */
package com.ming.common;

import com.ming.common.dynamic.code.IClassExecuter;
import com.ming.common.dynamic.code.IStringCompiler;
import com.ming.common.dynamic.code.RunSourceHandler;
import com.ming.common.dynamic.code.config.RunSourceProperties;
import com.ming.common.dynamic.code.core.ClassExecuter;
import com.ming.common.dynamic.code.core.JaninoCompiler;
import com.ming.common.dynamic.code.dto.ExecuteResult;
import com.ming.common.dynamic.code.dto.Parameters;
import com.ming.common.dynamic.code.exception.CompileException;
import com.ming.common.dynamic.code.exception.ExecuteException;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * 执行Java代码测试
 */
public class RunOtherHandlerTest {

    //@Test
    public void testHack() throws Exception {
        Map<String, String> hackers = new HashMap<>();
        hackers.put("java/io/File", "com/zhg2yqq/wheels/dynamic/code/hack/HackFile");
        RunSourceProperties calTime = new RunSourceProperties();
        IStringCompiler compiler = new JaninoCompiler();
        IClassExecuter<ExecuteResult> executer = new ClassExecuter();
        RunSourceHandler handler = new RunSourceHandler(compiler, executer, calTime, hackers);

        System.out.println("hack测试 start");
        Parameters pars0 = new Parameters();
        try {
            handler.runMethod(
                "package com.zhg2yqq.wheels.dynamic.code;\n" 
                + "import java.io.File;\n"
                + "public class FileTest {\n"
                + "    public void createFile() throws Exception {\n"
                + "        File file = new File(\"/zhg2yqq-test.txt\");\n"
                + "        file.createNewFile();\n"
                + "    }\n" 
                + "}",
                "createFile", pars0);
            Assert.assertFalse(true);
        } catch (ExecuteException e) {
            Throwable cause = e.getSourceCause();
            Assert.assertTrue(cause instanceof SecurityException);
        }
        System.out.println("hack测试 end");
    }

    //@Test
    public void testCompileException() throws Exception {
        Map<String, String> hackers = new HashMap<>();
        IStringCompiler compiler = new JaninoCompiler();
        IClassExecuter<ExecuteResult> executer = new ClassExecuter();
        RunSourceHandler handler = new RunSourceHandler(compiler, executer, new RunSourceProperties(), hackers);

        System.out.println("编译异常测试 start");
        Parameters pars0 = new Parameters();
        try {
            handler.runMethod(
                "package com.zhg2yqq.wheels.dynamic.code;\n" 
                + "import java.io.File;\n"
                + "public class FileTest {\n"
                + "    public void createFile() throws Exception {\n"
                + "        File file = new + File(\"/zhg2yqq-test.txt\");\n"
                + "        file.createNewFile();\n"
                + "    }\n" 
                + "}",
                "createFile", pars0);
            Assert.assertFalse(true);
        } catch (CompileException e) {
            Assert.assertTrue(true);
            System.out.println(e.getCompileMessage());
        }
        System.out.println("编译异常测试 end");
    }

    //@Test
    public void testExecuteException() throws Exception {
        Map<String, String> hackers = new HashMap<>();
        RunSourceProperties calTime = new RunSourceProperties();
        IStringCompiler compiler = new JaninoCompiler();
        IClassExecuter<ExecuteResult> executer = new ClassExecuter();
        RunSourceHandler handler = new RunSourceHandler(compiler, executer, calTime, hackers);

        System.out.println("源码运行异常测试 start");
        Parameters pars0 = new Parameters();
        try {
            handler.runMethod(
                "package com.zhg2yqq.wheels.dynamic.code;\n" 
                + "public class ExecuteTest {\n"
                + "    public void calc() {\n"
                + "        int a = 9;\n"
                + "        int b = 0;\n"
                + "        int c = a / b;\n"
                + "    }\n" 
                + "}",
                "calc", pars0);
            Assert.assertFalse(true);
        } catch (ExecuteException e) {
            Throwable cause = e.getSourceCause();
            Assert.assertTrue(cause instanceof ArithmeticException);
        }

        calTime.setExecuteTimeOut(5000);
        try {
            handler.runMethod(
                "package com.zhg2yqq.wheels.dynamic.code;\n" 
                + "import java.lang.InterruptedException;"
                + "public class ExecuteTest {\n"
                + "    public void calc() throws InterruptedException {\n"
                + "        int a = 9;\n"
                + "        int b = 1;\n"
                + "        Thread.sleep(10000);\n"
                + "        int c = a / b;\n"
                + "    }\n" 
                + "}",
                "calc", pars0, true, true);
            Assert.assertFalse(true);
        } catch (ExecuteException e) {
            Assert.assertTrue(e.getCause() instanceof TimeoutException);
        }
        System.out.println("源码运行测试 end");
    }

    /*@Test
    public void testStressTest() throws BaseDynamicException {
        Map<String, String> hackers = new HashMap<>();
        CalTimeDTO calTime = new CalTimeDTO();
        IStringCompiler compiler = new JaninoCompiler();
        IClassExecuter<ExecuteResult> executer = new ClassExecuter();
        RunSourceHandler handler = new RunSourceHandler(compiler, executer, calTime, hackers);

        System.out.println("压力测试 start");
        int stressTimes = 200;

        Parameters pars = new Parameters();
        pars.add("    测试     ");
        long current = System.currentTimeMillis();
        for (int i = 0; i < stressTimes; i++) {
            ExecuteResult result = handler.runMethod("package com.zhg2yqq.wheels.dynamic.code;\n"
                    + "public class CodeTemplate" + i + " {\n"
                    + "    public String trimStr(String str) {\n"
                    + "        return str.trim() + \"" + i + "\";\n"
                    + "    }\n"
                    + "}", "trimStr",
                    pars);
            Assert.assertEquals("测试" + i, result.getReturnVal());
        }
        System.out.println("0 cost time: " + (System.currentTimeMillis() - current));

        current = System.currentTimeMillis();
        for (int i = 0; i < stressTimes; i++) {
            ExecuteResult result = handler.runMethod("package com.zhg2yqq.wheels.dynamic.code;\n"
                    + "private class CodeTemplate {\n"
                    + "    public String trimStr(String str) {\n"
                    + "        return str.trim();\n"
                    + "    }\n"
                    + "}", "trimStr",
                    pars);
            Assert.assertEquals("测试", result.getReturnVal());
        }
        System.out.println("1 cost time: " + (System.currentTimeMillis() - current));
        System.out.println("压力测试 end");
    }*/
}