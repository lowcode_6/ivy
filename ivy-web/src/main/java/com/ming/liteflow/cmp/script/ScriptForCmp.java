package com.ming.liteflow.cmp.script;

import com.yomahub.liteflow.script.ScriptExecuteWrap;
import com.yomahub.liteflow.script.body.JaninoForScriptBody;

public class ScriptForCmp implements JaninoForScriptBody {
    @Override
    public Integer body(ScriptExecuteWrap scriptExecuteWrap) {
        System.out.println("ScriptForCmp executed!");
        return 5;
    }
}
