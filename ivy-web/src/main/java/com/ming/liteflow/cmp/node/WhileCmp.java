package com.ming.liteflow.cmp.node;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;

@LiteflowComponent("WhileCmp")
public class WhileCmp extends NodeBooleanComponent {
    @Override
    public boolean processBoolean() throws Exception {
        System.out.println("WhileCmp executed!");
        return false;
    }
}