package com.ming.liteflow.cmp.fallback;

import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;
@FallbackCmp
@LiteflowComponent("IfFallbackCmp")
public class IfFallbackCmp  extends NodeBooleanComponent {
    @Override
    public boolean processBoolean() throws Exception {
        System.out.println("IfFallbackCmp executed!");
        return true;
    }
}