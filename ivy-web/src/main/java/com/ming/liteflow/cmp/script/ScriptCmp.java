package com.ming.liteflow.cmp.script;

import com.yomahub.liteflow.script.ScriptExecuteWrap;
import com.yomahub.liteflow.script.body.JaninoCommonScriptBody;

public class ScriptCmp implements JaninoCommonScriptBody {
    @Override
    public Void body(ScriptExecuteWrap scriptExecuteWrap) {
        System.out.println("ScriptCmp executed!");
        return null;
    }
}
