/**
 * <p>Title: liteflow</p>
 * <p>Description: 轻量级的组件式流程框架</p>
 * @author Bryan.Zhang
 * @email weenyc31@163.com
 * @Date 2020/4/1
 */
package com.ming.liteflow.cmp.node;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;

@LiteflowComponent("BreakCmp")
public class BreakCmp extends NodeBooleanComponent {
	@Override
	public boolean processBoolean() {
		System.out.println("BreakCmp executed!");
		return true;
	}
}
