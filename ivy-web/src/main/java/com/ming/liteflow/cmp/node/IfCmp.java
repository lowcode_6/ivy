package com.ming.liteflow.cmp.node;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;

@LiteflowComponent("IfCmp")
public class IfCmp  extends NodeBooleanComponent {
    @Override
    public boolean processBoolean() throws Exception {
        System.out.println("IfCmp executed!");
        return true;
    }
}