package com.ming.liteflow.controller;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.thread.ThreadUtil;
import com.github.benmanes.caffeine.cache.Cache;
import com.ivy.vueflow.load.LiteFlowNodeLoad;
import com.ivy.vueflow.parser.entity.FlowDataVo;
import com.ivy.vueflow.parser.entity.node.Node;
import com.ivy.vueflow.parser.execption.LiteFlowELException;
import com.ivy.vueflow.parser.graph.Graph;
import com.ming.common.beetl.util.Result;
import com.ming.common.xxljob.annotation.PermissionLimit;
import com.ming.core.liteflow.debug.ACmp;
import com.ming.core.liteflow.debug.IvyCmpStep;
import com.ming.core.liteflow.debug.IvyContextBean;
import com.ming.core.log.LogInit;
import com.yomahub.liteflow.builder.LiteFlowNodeBuilder;
import com.yomahub.liteflow.builder.el.ELWrapper;
import com.yomahub.liteflow.builder.el.LiteFlowChainELBuilder;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.LiteflowResponse;
import org.beetl.sql.core.SQLManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/liteflow/debug")
public class LiteFlowDebugController {

    private static final Logger LOG = LoggerFactory.getLogger(LiteFlowDebugController.class);

    @Resource
    private SQLManager sqlManager;

    @Resource
    private FlowExecutor flowExecutor;

    @Autowired
    Cache<String, Map<String, IvyCmpStep>> ivyCmpStepCache;
    @Autowired
    Cache<String, String> ivyCmpLogCache;

    private Future<LiteflowResponse> response;

    private List<String> continueNodeList = ListUtil.toList("then","when");

    @PostMapping("/startingNodes")
    @PermissionLimit(limit = false)
    public Result<?> startingNodes(@RequestBody FlowDataVo data) throws Exception {
        Graph graph = new Graph(data);
        return Result.OK(graph.getStartNodes());
    }

    @PostMapping("/status")
    @PermissionLimit(limit = false)
    public Result<?> status(@RequestBody FlowDataVo data) throws Exception {
        String requestId = data.getRequestId();
        String nodeId = data.getNodeId();
        // 如果是THEN和WHEN节点、for指定次数，直接跳过
        if(continueNodeList.contains(data.getNodeType()) || ("for".equals(data.getNodeType()) && data.getLoopNumber() != null)){
            return Result.OK(new IvyCmpStep(nodeId, true, null));
        }

        while (true){
            Map<String, IvyCmpStep> stepMap = ivyCmpStepCache.getIfPresent(requestId);
            if(stepMap != null){
                if(response != null && response.isDone()){
                    IvyCmpStep ivyCmpStep = stepMap.get(nodeId);
                    if(ivyCmpStep == null){
                        ivyCmpStep = new IvyCmpStep(nodeId,false,null);
                    }
                    return Result.OK(ivyCmpStep);
                }else if(stepMap.containsKey(nodeId)){
                    return Result.OK(stepMap.get(nodeId));
                }
            }else if(response != null && response.isDone()){
                return Result.error(response.get().getMessage());
            }
            ThreadUtil.sleep(10);
        }
    }

    @PostMapping("/debug")
    @PermissionLimit(limit = false)
    public Result<?> debug(@RequestBody FlowDataVo data) throws LiteFlowELException {
        ThreadUtil.execAsync(()->{
            Graph graph = new Graph(data);
            String el = null;
            try {
                el = graph.toEL(data.getFormat());
            } catch (LiteFlowELException e) {
                throw new RuntimeException(e);
            }

            long ms = System.currentTimeMillis();
            String startKey = ms + LogInit.startKey;
            String endKey = ms + LogInit.endKey;
            LOG.info(startKey);

            // 构建Node
            try {
                LiteFlowNodeLoad.loadAndBuild(graph);
            } catch (LiteFlowELException e) {
                e.printStackTrace();
            }
            // 构建Chain
            String chainId = "test_chain"+ UUID.fastUUID().toString(true);
            LiteFlowChainELBuilder.createChain().setChainId(chainId).setEL(el).build();
            String requestId = data.getRequestId();
            IvyContextBean contextBean = new IvyContextBean(requestId);

            try {
                response = flowExecutor.execute2FutureWithRid(chainId, null,requestId, contextBean);
                LiteflowResponse liteflowResponse = response.get();
            } catch (Exception e) {
                System.out.println(ExceptionUtil.stacktraceToString(e));
                throw new RuntimeException(e);
            }
            LOG.info(endKey);
            List<String> list = LogInit.logMap.get(endKey);
            while (list == null || list.isEmpty()) {
                list = LogInit.logMap.get(endKey);// 等待list不为空
                ThreadUtil.sleep(50);
            }
            LogInit.logMap.remove(endKey);
            ivyCmpLogCache.put(requestId, String.join(System.lineSeparator(),list));
        });
        return Result.OK();
    }

    @PostMapping("/log")
    @PermissionLimit(limit = false)
    public Result<?> log(@RequestBody FlowDataVo data) throws Exception {
        String requestId = data.getRequestId();
        String logStr = ivyCmpLogCache.getIfPresent(requestId);
        return Result.OK(logStr);
    }
}
