package com.ming.liteflow.controller;

import cn.hutool.core.collection.CollUtil;
import com.ivy.vueflow.convert.FlowConvert;
import com.ivy.vueflow.convert.VueFlowConvert;
import com.ivy.vueflow.parser.entity.FlowData;
import com.ivy.vueflow.parser.graph.Graph;
import com.ivy.vueflow.parser.graph.GraphInfo;
import com.ivy.vueflow.parser.wrapper.ELBusWrapper;
import com.ming.common.beetl.util.Result;
import com.ming.common.util.ClassPathScanUtil;
import com.ming.common.xxljob.annotation.PermissionLimit;
import com.ming.core.liteflow.debug.IvyContextBean;
import com.ming.core.liteflow.entity.IvyDict;
import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.builder.LiteFlowNodeBuilder;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ELWrapper;
import com.yomahub.liteflow.builder.el.LiteFlowChainELBuilder;
import com.yomahub.liteflow.builder.el.ThenELWrapper;
import com.yomahub.liteflow.core.*;
import com.yomahub.liteflow.flow.LiteflowResponse;
import org.beetl.sql.core.SQLManager;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/liteflow/parser")
public class LiteFlowParserController {

    @Resource
    private SQLManager sqlManager;

    @Resource
    private FlowExecutor flowExecutor;

    @Autowired
    private ApplicationContext applicationContext;

    @PostMapping("/cmpOptions")
    @PermissionLimit(limit = false)
    public Result<?> cmpOptions(@RequestBody Map<String,Object> params) throws Exception {
        String type = (String) params.get("type");
        if(cn.hutool.core.util.StrUtil.isBlank(type)){
            return Result.OK(new ArrayList<>());
        }
        Map<String,List<String>> map = new LinkedHashMap<>();
        String basePackage = "com.ming";
        IvyDict ivyDict = sqlManager.lambdaQuery(IvyDict.class).andEq(IvyDict::getCode,"ivy_base_package").single();
        if(ivyDict != null && cn.hutool.core.util.StrUtil.isNotBlank(ivyDict.getCode())){
            basePackage = ivyDict.getValue();
        }
        Set<Class<?>> classesSet = new HashSet<>();
        switch (type){
            case "common":
                classesSet = ClassPathScanUtil.scanBySuper(NodeComponent.class, basePackage);
                break;
            case "switch":
                classesSet = ClassPathScanUtil.scanBySuper(NodeSwitchComponent.class, basePackage);
                break;
            case "if":
                classesSet = ClassPathScanUtil.scanBySuper(NodeBooleanComponent.class, basePackage);
                break;
            case "for":
                classesSet = ClassPathScanUtil.scanBySuper(NodeForComponent.class, basePackage);
                break;
            case "while":
                classesSet = ClassPathScanUtil.scanBySuper(NodeBooleanComponent.class, basePackage);
                break;
            case "break":
                classesSet = ClassPathScanUtil.scanBySuper(NodeBooleanComponent.class, basePackage);
                break;
            case "iterator":
                classesSet = ClassPathScanUtil.scanBySuper(NodeIteratorComponent.class, basePackage);
                break;
            default:
                classesSet = ClassPathScanUtil.scanByAnno(LiteflowComponent.class, basePackage);
                break;
        }
        // 过滤掉降级组件
        classesSet = classesSet.stream().filter(m->!m.isAnnotationPresent(FallbackCmp.class)).collect(Collectors.toSet());

        Map<String, Object> beansWithAnnotation = applicationContext.getBeansWithAnnotation(LiteflowComponent.class);
        List<String> annoList = beansWithAnnotation.values().stream().filter(m->!m.getClass().isAnnotationPresent(FallbackCmp.class)).map(m->m.getClass().getName()).collect(Collectors.toList());

        //0：普通类，1：springbean,2：动态类，3：脚本
        map.put("c0", classesSet.stream().map(m->m.getName()).collect(Collectors.toList()));
        map.put("c1", annoList);
//        map.put("c2", null);
//        map.put("c3", null);
        return Result.OK(map);
    }

    @PostMapping("/jsonToEL")
    @PermissionLimit(limit = false)
    public Result<?> jsonToEL(@RequestBody FlowData data) throws Exception {
        try {
            Graph graph = new Graph(data);
            GraphInfo graphInfo = graph.toELInfo();
            String result = graphInfo.toString();
            System.out.println(result);
            return Result.OK(result);
        }catch (Exception e){
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }

    @PostMapping("/elToJson")
    @PermissionLimit(limit = false)
    public Result<?> elToJson(@RequestBody Map<String,Object> params) throws Exception {
        FlowConvert convert = new VueFlowConvert();
        String json = convert.el2Json((String) params.get("el"));
        return Result.OK(json);
    }

    //获取异常类、错误类全路径
    @PostMapping("/getRetryExceptions")
    @PermissionLimit(limit = false)
    public Result<?> getRetryExceptions(@RequestBody Map<String,Object> params) throws Exception {
        Reflections reflections = new Reflections();
        Set<Class<? extends Throwable>> classes = reflections.getSubTypesOf(Throwable.class);
        classes.forEach(clazz -> System.out.println(clazz.getName()));
        return Result.OK();
    }

}

