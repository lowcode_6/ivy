package com.ming.monitor;

import com.ming.monitor.controller.IvyMonitorJVMController;
import com.ming.monitor.controller.IvyMonitorOSController;
import org.beifengtz.jvmm.core.JvmmCollector;
import org.beifengtz.jvmm.core.JvmmExecutor;
import org.beifengtz.jvmm.core.JvmmFactory;
import org.beifengtz.jvmm.core.JvmmProfiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class IvyMonitorInit {

    @Autowired
    private IvyMonitorOSController ivyMonitorOSController;
    @Autowired
    private IvyMonitorJVMController ivyMonitorJVMController;

    @PostConstruct
    public void init(){
        //    提供所有的数据采集接口
        //JvmmCollector collector = JvmmFactory.getCollector();
        //    提供所有的执行接口
        //JvmmExecutor executor = JvmmFactory.getExecutor();
        //    提供火焰图生成器
        //JvmmProfiler profiler = JvmmFactory.getProfiler();

        //ivyMonitorOSController.info(null);
        //ivyMonitorJVMController.info(null);
    }

}
