package com.ming.common.liteflow.core.el.prop;

import lombok.Data;

@Data
public class Properties {

    private String id;
    private String tag;
    private Integer maxWaitSeconds;

}
