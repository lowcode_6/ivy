package com.ming.common.liteflow.vo;

import com.ming.core.query.Options;
import com.ming.core.liteflow.entity.IvyChain;
import lombok.Data;

@Data
public class IvyChainVo extends IvyChain {

    private Options options;

}
