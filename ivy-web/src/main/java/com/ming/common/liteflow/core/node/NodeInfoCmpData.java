package com.ming.common.liteflow.core.node;

import lombok.Data;

@Data
public class NodeInfoCmpData {

    private String dataName;

    private String data;
}
