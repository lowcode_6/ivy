package com.ming.common.liteflow.core.el.bus;

import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.WhenELWrapper;

public class ELBusWhen {

    private WhenELWrapper wrapper = ELBus.when();

    public static ELBusWhen NEW(){
        return new ELBusWhen();
    }

//    public ELBusWhen node(Node... nodes){
//        for (Node node : nodes){
//            wrapper.when(node.getProperties().getComponentId());
//        }
//        return this;
//    }

    public WhenELWrapper toELWrapper(){
        return wrapper;
    }

}
