package com.ming.common.liteflow.core.execption;

public class LiteFlowELException extends Exception {

    public LiteFlowELException(String message) {
        super(message);
    }

}
