package com.ming.common.liteflow.core.el.prop;

import lombok.Data;

@Data
public class WhileProperties extends Properties {

    private Boolean parallel;
    private String doOpt;
    private String breakOpt;

}
