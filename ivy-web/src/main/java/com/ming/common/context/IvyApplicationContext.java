package com.ming.common.context;

import cn.hutool.extra.spring.SpringUtil;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IvyApplicationContext {

//    private Class configClass;
//    private Map<String, BeanDefinition> beanDefinitionMap = new HashMap<>();
//    private Map<String, Object> singletonObjects = new HashMap<>();
//    private List<BeanPostProcessor> beanPostProcessorList = new ArrayList<>();

    public void IvyApplicationContext() {

    }

    // 部署
    public void deploy(String json) {

    }

    // 加载
    public void load() {

    }

    // 注册bean
    public Object registerBean(String beanName, String source) {
        Object bean = null;
        SpringUtil.registerBean(beanName, bean);

        return bean;
    }

    // 获取bean
    public Object getBean(String beanName) {
        Object bean = SpringUtil.getBean(beanName);
        if(bean == null){
            bean = registerBean(beanName, null);
        }
        return bean;
    }

    // 销毁bean
    public void unregisterBean(String beanName) {
        SpringUtil.unregisterBean(beanName);
    }

}
