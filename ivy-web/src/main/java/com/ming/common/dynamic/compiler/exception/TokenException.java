/*
 * Copyright (c) zhg2yqq Corp.
 * All Rights Reserved.
 */
package com.ming.common.dynamic.compiler.exception;

/**
 * 词法分析异常
 * @version zhg2yqq v1.0
 * @author 周海刚, 2022年7月27日
 */
public class TokenException extends CompileException {
    private static final long serialVersionUID = 1L;
    private String charSequence;
    
    public TokenException(char c) {
        this.charSequence = Character.toString(c);
    }
    
    public TokenException(String charSequence) {
        this.charSequence = charSequence;
    }
}
