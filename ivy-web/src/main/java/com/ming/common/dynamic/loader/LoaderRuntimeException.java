package com.ming.common.dynamic.loader;

public class LoaderRuntimeException extends RuntimeException {
    public LoaderRuntimeException(String message) {
        super(message);
    }

    public LoaderRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
