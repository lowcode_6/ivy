//package com.ming.common.dynamic.spring.controller;
//
//
//import com.ming.core.dynamic.spring.core.MemoryClassLoader;
//import com.ming.core.dynamic.spring.util.FileUtil;
//import com.ming.core.dynamic.spring.util.SpringBeanUtil;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.lang.reflect.InvocationTargetException;
//import java.net.MalformedURLException;
//
///**
// * @author moon
// * @date 2023-08-30 14:10
// * @since 1.8
// */
//@RestController
//@RequestMapping("/reload")
//public class Reload extends ClassLoader{
//
//
//    @GetMapping("/re")
//    public void re(String name,String beanName) throws InstantiationException, IllegalAccessException, InvocationTargetException, MalformedURLException, NoSuchMethodException, ClassNotFoundException {
//
//        String className = "com.example.service.impl." + name;
//        String classPath = "C:\\Users\\zhanghx19\\Desktop\\jar\\"+name+".java";
//        String javaStr = FileUtil.readJson(classPath);
//
//        /**
//         * 定义新的 MemoryClassLoader 同一个 MemoryClassLoader 不能两次加载同一个类
//         */
//        MemoryClassLoader loader = new MemoryClassLoader();
//        loader.registerJava(className,javaStr);
//        loader.registerJava(javaStr);
//        Class clazz = loader.findClass(className);
//
////        TestAbstract handler = (TestAbstract) clazz.getDeclaredConstructor().newInstance();
////
////        // 将外部Jar包中的Bean注入到Spring容器中
////
////        Object obj = SpringBeanUtil.replace(beanName,clazz);
////
////        TestAbstract test = (TestAbstract) obj;
////
////        test.hand("sss");
//
//        System.out.println();
//
//    }
//
//}
