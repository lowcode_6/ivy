//package com.ming.common.dynamic.spring.controller;
//
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//
///**
// * @author moon
// * @date 2023-08-30 17:27
// * @since 1.8
// */
//@RestController
//@RequestMapping("/test")
//public class TestController {
//
//    /**
//     * 引入抽象类依赖
//     */
////    @Resource(name = "testAbstractImpl")
////    TestAbstract testAbstract;
////
////    @GetMapping("/print")
////    public void print(String content){
////        testAbstract.hand("Hello : " + content);
////    }
//}
