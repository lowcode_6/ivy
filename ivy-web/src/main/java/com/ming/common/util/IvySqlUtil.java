package com.ming.common.util;

import cn.hutool.extra.spring.SpringUtil;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.query.LambdaQuery;

public class IvySqlUtil<T> {

    private Class<T> clazz;
    private String tableName;
    private SQLManager sqlManager;
    private LambdaQuery<T> lambdaQuery;

    private IvySqlUtil() {}

    public IvySqlUtil(Class<T> clazz, SQLManager sqlManager) {
        this.clazz = clazz;
        this.tableName = sqlManager.getNc().getTableName(clazz);
        this.sqlManager = sqlManager;
    }

    public static <T> IvySqlUtil<T> sqlManager(){
        SQLManager sqlManager = SpringUtil.getBean("sqlManager");
        return sqlManager(sqlManager);
    }

    public static <T> IvySqlUtil<T> sqlManager(Class<T> clazz){
        SQLManager sqlManager = SpringUtil.getBean("sqlManager");
        return sqlManager(clazz, sqlManager);
    }

    public static <T> IvySqlUtil<T> sqlManager(Class<T> clazz, SQLManager sqlManager){
        return new IvySqlUtil<>(clazz, sqlManager);
    }

    public static <T> IvySqlUtil<T> sqlManager(SQLManager sqlManager){
        return new IvySqlUtil<>(null, sqlManager);
    }

    public static void main(String[] args) {
        //IvySqlUtil.sqlManager().lambdaQuery();
    }
}
