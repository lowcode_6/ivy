package com.ming.common.util;

import cn.hutool.crypto.symmetric.SymmetricCrypto;

public class CryptoUtil {

    private static byte[] aesKey = new byte[]{50,-85,-10,-86,-13,87,-105,-110,-44,121,-91,-63,-68,-110,29,-31};

    private static SymmetricCrypto aes;

    private volatile static CryptoUtil instance;

    private CryptoUtil() {
        aes = new SymmetricCrypto("AES", aesKey);
    }

    public static CryptoUtil NEW(){
        if(instance == null){
            synchronized (CryptoUtil.class){
                if(instance == null){
                    instance = new CryptoUtil();
                }
            }
        }
        return instance;
    }

    public String encrypt(String str){
        return aes.encryptBase64(str);
    }

    public String decrypt(String str){
        return aes.decryptStr(str);
    }

    public static void main(String[] args) {
        String encrypt = CryptoUtil.NEW().encrypt("123456");
        System.out.println(encrypt);
        String decrypt = CryptoUtil.NEW().decrypt(encrypt);
        System.out.println(decrypt);
    }
}
