package com.ming.common.beetl.execption;

public class SQLBuildException extends Exception {

    public SQLBuildException(String message) {
        super(message);
    }

}
