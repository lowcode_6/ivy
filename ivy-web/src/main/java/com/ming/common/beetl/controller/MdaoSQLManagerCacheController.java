package com.ming.common.beetl.controller;

import com.github.benmanes.caffeine.cache.Cache;
import com.ming.common.beetl.cache.CacheSqlManagerUtil;
import com.ming.common.beetl.util.Result;
import org.beetl.sql.core.SQLManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sqlManager/cache")
public class MdaoSQLManagerCacheController {

    @PostMapping("/build")
    public Result build(){
        return Result.OK();
    }

    @GetMapping("/selectAll")
    public Result selectAll(){
        Cache<String, SQLManager> cacheList = CacheSqlManagerUtil.getCacheList();
        return Result.OK(cacheList);
    }

//    @PostMapping("/insert")
//    public Result insert(@RequestBody Map<String,Object> data) {
//        return Result.OK(mdaoSQLManagerService.insert(data));
//    }
//
//    @PostMapping("/updateById")
//    public Result updateById(@RequestBody Map<String,Object> data) {
//        return Result.OK(mdaoSQLManagerService.updateById(data));
//    }
//
//    @PostMapping("/deleteById")
//    public Result deleteById(@RequestBody Map<String,Object> data) {
//        return Result.OK(mdaoSQLManagerService.deleteById(data));
//    }
}
