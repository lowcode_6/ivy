package com.ming.common.beetl.vo;

import com.ming.core.query.Options;
import lombok.Data;

@Data
public class IvyDbTableVo {

    private Options options;

    private Long id;

    private String tableName;

}
