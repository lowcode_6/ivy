package com.ming.common.beetl.controller;

import com.ming.common.beetl.service.MdaoDataSourceService;
import com.ming.common.beetl.util.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/dataSource")
public class MdaoDataSourceController {

    @Resource
    private MdaoDataSourceService mdaoDataSourceService;

    @GetMapping("/selectAll")
    public Result selectAll(){
        return Result.OK(mdaoDataSourceService.selectAll());
    }

    @PostMapping("/insert")
    public Result insert(@RequestBody Map<String,Object> data) {
        return Result.OK(mdaoDataSourceService.insert(data));
    }

    @PostMapping("/updateById")
    public Result updateById(@RequestBody Map<String,Object> data) {
        return Result.OK(mdaoDataSourceService.updateById(data));
    }

    @PostMapping("/deleteById")
    public Result deleteById(@RequestBody Map<String,Object> data) {
        return Result.OK(mdaoDataSourceService.deleteById(data));
    }
}
