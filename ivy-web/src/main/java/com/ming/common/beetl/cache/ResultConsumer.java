package com.ming.common.beetl.cache;

@FunctionalInterface
public interface ResultConsumer<T> {
    Object accept(T t);
}
