package com.ming.common.beetl.entity;

import com.ming.core.dynamic.beetlsql.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.beetl.sql.core.db.DBStyle;

import javax.sql.DataSource;

@Setter
@Getter
@NoArgsConstructor
public class SQLManagerEntity {

    private DataSource dataSource;
    private DataSource[] slaveDataSources;
    private DBStyle dbStyle;
    private String initSql;

    private IvyDbSqlManager ivyDbSqlManager;
    private BaseEntity baseEntity;

}
