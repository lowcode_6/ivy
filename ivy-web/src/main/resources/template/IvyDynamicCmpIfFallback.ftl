package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;

@FallbackCmp
@LiteflowComponent("IvyDynamicCmpIfFallback")
public class IvyDynamicCmpIfFallback extends NodeBooleanComponent {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpIfFallback.class);

    @Override
    public boolean processBoolean() throws Exception {
        LOG.info("IvyDynamicCmpIfFallback executed!");
        System.out.println("IvyDynamicCmpIfFallback executed!");
        return true;
    }
}