package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeForComponent;

@LiteflowComponent("IvyDynamicCmpFor")
public class IvyDynamicCmpFor extends NodeForComponent {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpFor.class);

    @Override
    public int processFor() throws Exception {
        LOG.info("IvyDynamicCmpFor executed!");
        System.out.println("IvyDynamicCmpFor executed!");
        return 3;
    }
}