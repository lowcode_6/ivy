package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;

@FallbackCmp
@LiteflowComponent("IvyDynamicCmpBreakFallback")
public class IvyDynamicCmpBreakFallback extends NodeBooleanComponent {

	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpBreakFallback.class);

	@Override
	public boolean processBoolean() {
		LOG.info("IvyDynamicCmpBreakFallback executed!");
		System.out.println("IvyDynamicCmpBreakFallback executed!");
		return true;
	}
}
