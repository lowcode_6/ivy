package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;

@FallbackCmp
@LiteflowComponent("IvyDynamicCmpWhileFallback")
public class IvyDynamicCmpWhileFallback extends NodeBooleanComponent {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpWhileFallback.class);

    @Override
    public boolean processBoolean() throws Exception {
        LOG.info("IvyDynamicCmpWhileFallback executed!");
        System.out.println("IvyDynamicCmpWhileFallback executed!");
        return false;
    }
}