package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;

@LiteflowComponent("IvyDynamicCmpWhile")
public class IvyDynamicCmpWhile extends NodeBooleanComponent {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpWhile.class);

    @Override
    public boolean processBoolean() throws Exception {
        LOG.info("IvyDynamicCmpWhile executed!");
        System.out.println("IvyDynamicCmpWhile executed!");
        return false;
    }
}