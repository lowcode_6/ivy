package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;

@LiteflowComponent("IvyDynamicCmpBreak")
public class IvyDynamicCmpBreak extends NodeBooleanComponent {

	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpBreak.class);

	@Override
	public boolean processBoolean() {
		LOG.info("IvyDynamicCmpBreak executed!");
		System.out.println("IvyDynamicCmpBreak executed!");
		return true;
	}
}
