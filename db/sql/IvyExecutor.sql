-- 建表语句
CREATE TABLE `ivy_executor` (
`id` BIGINT(20)  NOT NULL AUTO_INCREMENT ,
`executor_id` VARCHAR(32)  DEFAULT NULL  COMMENT '执行器ID',
`executor_name` VARCHAR(32)  DEFAULT NULL  COMMENT '执行器名称',
`executor_type` VARCHAR(12)  DEFAULT NULL  COMMENT '执行器类型【execute2Resp:execute2Resp,execute2Future:execute2Future】',
`ivy_config_id` BIGINT(20)  DEFAULT NULL  COMMENT '执行器配置IvyConfig',
`context_beans` VARCHAR(32)  DEFAULT NULL  COMMENT '上下文参数，ivy_dynamic_class表ID',
PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- 新增字段
ALTER TABLE `ivy_executor` ADD COLUMN id BIGINT(20)  DEFAULT NULL ;
ALTER TABLE `ivy_executor` ADD COLUMN executor_id VARCHAR(32)  DEFAULT NULL  COMMENT '执行器ID';
ALTER TABLE `ivy_executor` ADD COLUMN executor_name VARCHAR(32)  DEFAULT NULL  COMMENT '执行器名称';
ALTER TABLE `ivy_executor` ADD COLUMN executor_type VARCHAR(12)  DEFAULT NULL  COMMENT '执行器类型【execute2Resp:execute2Resp,execute2Future:execute2Future】';
ALTER TABLE `ivy_executor` ADD COLUMN ivy_config_id BIGINT(20)  DEFAULT NULL  COMMENT '执行器配置IvyConfig';
ALTER TABLE `ivy_executor` ADD COLUMN context_beans VARCHAR(32)  DEFAULT NULL  COMMENT '上下文参数，ivy_dynamic_class表ID';

