-- 建表语句
CREATE TABLE `ivy_dynamic_class` (
`id` BIGINT(20)  NOT NULL AUTO_INCREMENT ,
`class_id` VARCHAR(128)  DEFAULT NULL  COMMENT '动态类ID',
`class_type` INTEGER(2)  DEFAULT NULL  COMMENT 'class类型【0:普通类,1:普通组件类,2:选择组件类,3:条件组件类,4:次数循环组件类,5:条件循环组件类,6:迭代循环组件类,7:退出循环组件类】',
`is_fallback` INTEGER(1)  DEFAULT NULL  COMMENT '是否为降级组件【0:非降级组件,1:降级组件】',
`package_path` VARCHAR(32)  DEFAULT NULL  COMMENT '包路径',
`class_name` VARCHAR(32)  DEFAULT NULL  COMMENT '动态类名称',
`class_loader_type` INTEGER(1)  DEFAULT NULL  COMMENT '加载器类型【0:应用程序类加载器AppClassLoader,1:自定义类加载器HotSwapClassLoader】',
`source_code` TEXT  DEFAULT NULL  COMMENT 'Java源码',
`version` INTEGER(11)  DEFAULT NULL  COMMENT '版本号',
`source_cmp_id` VARCHAR(32)  DEFAULT NULL  COMMENT '组件ID',
`source_class_name` VARCHAR(32)  DEFAULT NULL  COMMENT '源码类名',
PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- 新增字段
ALTER TABLE `ivy_dynamic_class` ADD COLUMN id BIGINT(20)  DEFAULT NULL ;
ALTER TABLE `ivy_dynamic_class` ADD COLUMN class_id VARCHAR(128)  DEFAULT NULL  COMMENT '动态类ID';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN class_type INTEGER(2)  DEFAULT NULL  COMMENT 'class类型【0:普通类,1:普通组件类,2:选择组件类,3:条件组件类,4:次数循环组件类,5:条件循环组件类,6:迭代循环组件类,7:退出循环组件类】';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN is_fallback INTEGER(1)  DEFAULT NULL  COMMENT '是否为降级组件【0:非降级组件,1:降级组件】';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN package_path VARCHAR(32)  DEFAULT NULL  COMMENT '包路径';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN class_name VARCHAR(32)  DEFAULT NULL  COMMENT '动态类名称';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN class_loader_type INTEGER(1)  DEFAULT NULL  COMMENT '加载器类型【0:应用程序类加载器AppClassLoader,1:自定义类加载器HotSwapClassLoader】';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN source_code TEXT  DEFAULT NULL  COMMENT 'Java源码';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN version INTEGER(11)  DEFAULT NULL  COMMENT '版本号';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN source_cmp_id VARCHAR(32)  DEFAULT NULL  COMMENT '组件ID';
ALTER TABLE `ivy_dynamic_class` ADD COLUMN source_class_name VARCHAR(32)  DEFAULT NULL  COMMENT '源码类名';

