docker stop ivynginx

docker rm ivynginx

docker run --name ivynginx -p 80:80 -v /root/ivy/nginx.conf:/etc/nginx/nginx.conf:ro -v /root/ivy/html:/usr/share/nginx/html:ro -d nginx