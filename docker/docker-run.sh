#!/bin/bash

# shellcheck disable=SC2046
docker rm $(docker stop ivy-web)

# 打包
docker build -t ivy-web:1.0.0 .

# run
docker run -dit --restart=always --name=ivy-web  -p 8080:8080 -v /etc/localtime:/etc/localtime ivy-web:1.0.0


docker stop ivynginx

docker rm ivynginx

docker run --name ivynginx -p 80:80 -v /root/ivy/nginx.conf:/etc/nginx/nginx.conf:ro -v /root/ivy/html:/usr/share/nginx/html:ro -d nginx


docker logs -f ivy-web