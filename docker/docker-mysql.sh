#!/bin/bash

docker stop mysql8

docker rm mysql8

#使用以下命令运行MySQL 8的Docker容器，并设置root用户的密码为123456：
docker run --restart always -p 3306:3306 --name mysql8 -e MYSQL_ROOT_PASSWORD=123456 -v /root/ivy/data:/var/lib/mysql:Z -d mysql:8
