#!/bin/bash

# shellcheck disable=SC2046
docker rm $(docker stop ivy-web)

# 打包
docker build -t ivy-web:1.0.0 .

# run
docker run -dit --restart=always --name=ivy-web  -p 8080:8080 -v /etc/localtime:/etc/localtime ivy-web:1.0.0

docker logs -f ivy-web