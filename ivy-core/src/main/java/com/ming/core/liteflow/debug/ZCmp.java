package com.ming.core.liteflow.debug;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("z")
public class ZCmp extends NodeComponent {

    @Override
    public void process() {
        System.out.println("ZCmp executed！");
    }
}
