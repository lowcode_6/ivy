package com.ming.core.liteflow.debug;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class IvyCmpStep {

    private String nodeId;
    private Boolean isSuccess;
    private Exception exception;

}
