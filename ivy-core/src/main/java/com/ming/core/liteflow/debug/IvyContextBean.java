package com.ming.core.liteflow.debug;

import lombok.Data;

@Data
public class IvyContextBean {

    private String requestId;


    public IvyContextBean(String requestId) {
        this.requestId = requestId;
    }
}
