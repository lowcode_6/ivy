package com.ming.core.liteflow.debug;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@Configuration
public class LiteFlowCacheConfig {

//    @Resource
//    private SQLManager sqlManager;

    @Bean("ivyCmpStepCache")
    public @NonNull Cache<String, Map<String,IvyCmpStep>> ivyCmpStepCache() {
        return Caffeine.newBuilder().initialCapacity(10).maximumSize(1000)
                .expireAfterWrite(10, TimeUnit.MINUTES).build();
    }

    @Bean("ivyCmpLogCache")
    public @NonNull Cache<String, String> ivyCmpLogCache() {
        return Caffeine.newBuilder().initialCapacity(10).maximumSize(1000)
                .expireAfterWrite(10, TimeUnit.MINUTES).build();
    }

}
