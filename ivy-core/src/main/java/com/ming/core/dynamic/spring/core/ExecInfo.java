package com.ming.core.dynamic.spring.core;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExecInfo {

    private String returnVal;
    private List<String> logList;
    private String logStr;
    private String el;

}
