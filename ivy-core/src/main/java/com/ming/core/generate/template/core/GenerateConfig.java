package com.ming.core.generate.template.core;


import com.ming.core.generate.template.utils.PropertyUtil;

import java.util.Map;

/**
 * Created by Administrator on 2020/4/13 0013.
 */
public class GenerateConfig {

    private static Map<String,String> map = PropertyUtil.getProperty("/generate.properties");

    private static final String voTemplateKey = "generate.vo.template.path";
    private static final String controllerTemplateKey = "generate.controller.template.path";
    private static final String serviceTemplateKey = "generate.service.template.path";
    private static final String serviceImplTemplateKey = "generate.service.impl.template.path";
    private static final String mapperTemplateKey = "generate.mapper.template.path";
    private static final String mapperXmlTemplateKey = "generate.mapper.xml.template.path";
    private static final String sqlTemplateKey = "generate.sql.template.path";

    public static String getVoTemplatePath(){return get(voTemplateKey);}
    public static String getControllerTemplatePath(){return get(controllerTemplateKey);}
    public static String getServiceTemplatePath(){return get(serviceTemplateKey);}
    public static String getServiceImplTemplatePath(){return get(serviceImplTemplateKey);}
    public static String getMapperTemplatePath(){return get(mapperTemplateKey);}
    public static String getMapperXmlTemplatePath(){return get(mapperXmlTemplateKey);}
    public static String getSqlTemplatePath(){return get(sqlTemplateKey);}


    private static final String voSrcKey = "generate.vo.src.path";
    private static final String controllerSrcKey = "generate.controller.src.path";
    private static final String serviceSrcKey = "generate.service.src.path";
    private static final String serviceImplSrcKey = "generate.service.impl.src.path";
    private static final String mapperSrcKey = "generate.mapper.src.path";
    private static final String mapperXmlSrcKey = "generate.mapper.xml.src.path";
    private static final String sqlSrcKey = "generate.sql.src.path";

    public static String getVoSrcPath(){return get(voSrcKey);}
    public static String getControllerSrcPath(){return get(controllerSrcKey);}
    public static String getServiceSrcPath(){return get(serviceSrcKey);}
    public static String getServiceImplSrcPath(){return get(serviceImplSrcKey);}
    public static String getMapperSrcPath(){return get(mapperSrcKey);}
    public static String getMapperXmlSrcPath(){return get(mapperXmlSrcKey);}
    public static String getSqlSrcPath(){return get(sqlSrcKey);}


    public static String get(String key){
        if(map.containsKey(key)){
            return map.get(key);
        }
        return "";
    }
}
