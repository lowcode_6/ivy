package com.ivy.executor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@ComponentScans(@ComponentScan(basePackages = "com.ming"))
@SpringBootApplication
public class IvyExecutor2Application {

    public static void main(String[] args) {
        SpringApplication.run(IvyExecutor2Application.class, args);
    }

}
